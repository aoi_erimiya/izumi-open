# izumibot-open

Google App Script 上で動作する Line Bot。

本当は拡張子 gs だけどコード補完とか利かせるために js として保存してます。
プルリク歓迎です！

※外部サービスの API 呼び出しやスクレイピングを行っていますが
　取り扱いにはご注意ください。

このソースコードは MIT ライセンスの元に配布しています。
This software is released under the MIT License, see LICENSE.txt.
